export { default as HinBubble } from './src/components/HinBubble.vue';

// directives
export { ClickOutside } from './src/directives/ClickOutside/ClickOutside';

// methods
export { playScenario } from './src/methods/playScenario';
export { targetElementStep } from './src/methods/playScenario';
