import HintedIcon from '../assets/icons/HintedIcon';
import '../methods/playScenario';
import __vue_normalize__ from 'vue-runtime-helpers/dist/normalize-component.js';
import __vue_create_injector__ from 'vue-runtime-helpers/dist/inject-style/browser.mjs';

//
var script = {
  name: 'HinBubble',
  props: {
    visible: {
      type: Boolean,
      required: false,
      default: false
    },
    rect: {
      type: DOMRect,
      required: false,
      default: function _default() {}
    },
    position: {
      type: String,
      required: false,
      default: 'top'
    }
  },
  components: {
    HintedIcon: HintedIcon
  },
  computed: {
    styles: function styles() {
      var _this$rect, _this$rect2;

      console.log(this.rect, 5555);
      return {
        left: "".concat((_this$rect = this.rect) === null || _this$rect === void 0 ? void 0 : _this$rect.left, "px") || 0,
        top: "".concat((_this$rect2 = this.rect) === null || _this$rect2 === void 0 ? void 0 : _this$rect2.top, "px") || 0
      };
    }
  }
};

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.visible)?_c('div',{staticClass:"hin-bubble",style:(_vm.styles)},[_c('div',{staticClass:"hin-bubble__title"}),_vm._v(" "),_c('div',{staticClass:"hin-bubble__description"}),_vm._v(" "),_c('div',{staticClass:"hin-bubble__footer"},[_c('div',{staticClass:"hin-bubble__label"},[_vm._v("Power by "),_c('HintedIcon')],1),_vm._v(" "),_c('div',{staticClass:"hin-bubble__steps"},[_vm._v("step 1 of 30")]),_vm._v(" "),_vm._m(0)])]):_vm._e()};
var __vue_staticRenderFns__ = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"hin-bubble__buttons"},[_c('button',{staticClass:"hin-bubble__button"},[_vm._v("123")])])}];

  /* style */
  const __vue_inject_styles__ = function (inject) {
    if (!inject) return
    inject("data-v-efa1d58c_0", { source: ".hin-bubble[data-v-efa1d58c]{background:#fff;box-shadow:0 10px 25px rgba(0,0,0,.2);border-radius:10px;padding:20px;max-width:300px;width:100%;position:fixed}.hin-bubble__title[data-v-efa1d58c]{font-size:20px;line-height:23px;color:#343347;margin-bottom:10px}.hin-bubble__description[data-v-efa1d58c]{font-size:15px;line-height:18px;color:#343347}.hin-bubble__footer[data-v-efa1d58c]{margin-top:15px;display:flex;align-items:center;justify-content:space-between}.hin-bubble__label[data-v-efa1d58c]{font-size:10px;line-height:12px;color:#bbbbc6}.hin-bubble__steps[data-v-efa1d58c]{font-size:12px;line-height:14px;color:#bbbbc6}", map: undefined, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__ = "data-v-efa1d58c";
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject SSR */
  
  /* style inject shadow dom */
  

  
  const __vue_component__ = /*#__PURE__*/__vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    false,
    __vue_create_injector__,
    undefined,
    undefined
  );

export { __vue_component__ as default };
