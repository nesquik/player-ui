function _readOnlyError(name) {
  throw new TypeError("\"" + name + "\" is read-only");
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _createForOfIteratorHelper(o, allowArrayLike) {
  var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"];

  if (!it) {
    if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
      if (it) o = it;
      var i = 0;

      var F = function () {};

      return {
        s: F,
        n: function () {
          if (i >= o.length) return {
            done: true
          };
          return {
            done: false,
            value: o[i++]
          };
        },
        e: function (e) {
          throw e;
        },
        f: F
      };
    }

    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  var normalCompletion = true,
      didErr = false,
      err;
  return {
    s: function () {
      it = it.call(o);
    },
    n: function () {
      var step = it.next();
      normalCompletion = step.done;
      return step;
    },
    e: function (e) {
      didErr = true;
      err = e;
    },
    f: function () {
      try {
        if (!normalCompletion && it.return != null) it.return();
      } finally {
        if (didErr) throw err;
      }
    }
  };
}

var ACTION_ELEMENTS = ['a', 'button'];
var playScenario = function playScenario(scenario) {
  var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var target = targetElementStep(scenario, index);

  if (!target) {
    console.error('Элемент не найден');
    return false;
  }

  console.log('Сценарий запущен');
  return target.getBoundingClientRect();
};
var targetElementStep = function targetElementStep(scenario, index) {
  var step = scenario.steps[index];
  var selector = step.target || step.selector;
  var stepDescription = step.elementDescription; // just one target to test: check and return its results

  var experimental = step.experimental ? step.experimental : null;
  var target = getTargetStep(selector, false, experimental);

  if (!target && stepDescription && stepDescription.selectors) {
    var secondarySelectors = stepDescription.selectors;
    target = getElementBySelectors(stepDescription.selectors, experimental);
    var parentSelector = secondarySelectors[secondarySelectors.length - 1];
    console.log(target, 'Временный лог', 1);

    if (!parentSelector) {
      return null;
    }

    target = getTargetStep(parentSelector, false, experimental);
    console.log(target, 'Временный лог', 2);

    if (!target) {
      return null;
    }

    var elPosition = stepDescription.position;

    if (Number.isInteger(elPosition)) {
      target = target.childNodes[elPosition] || target;
    }
  }

  console.log(getParentElementFromActionElements(target), target, 'Временный лог');
  return getParentElementFromActionElements(target) || target;
};
/**
 * Получить елемент выбранный в шаге
 * @param {string} selector Селектор элемента
 * @param {boolean} [debug = true] статуст тестирования
 * @param {object} [experimental = null]
 * @returns {Document|Element}
 * @private
 */

var getTargetStep = function getTargetStep(selector) {
  var debug = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var experimental = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

  if (!selector) {
    return null;
  }

  if (experimental && experimental.useInnerText && experimental.innerText) {
    return innerTextQS(document, selector, experimental.innerText);
  } // Поиск элемента в Iframe


  if (selector.indexOf(';;') < 0) {
    return advancedQS(document, selector);
  }

  debug && console.log('Searching element in iframes...');
  var subSelectors = selector.split(';;');
  var elementSelector = subSelectors.splice(subSelectors.length - 1, 1)[0];
  var iframeDocument = document;
  var paddingTop = 0;
  var paddingLeft = 0;

  var _getParamsIframe = getParamsIframe(subSelectors, iframeDocument),
      top = _getParamsIframe.top,
      left = _getParamsIframe.left,
      doc = _getParamsIframe.doc;

  paddingTop += top;
  paddingLeft += left;
  iframeDocument = doc;
  var element = advancedQS(iframeDocument, elementSelector);

  if (!element) {
    debug && console.warn(selector + ' element not found');
    return null;
  }

  element.isInsideIframe = true;
  element.elementPadding = {
    top: paddingTop,
    left: paddingLeft
  };
  return element;
};
/**
 * Поиск элемент по тексту
 * @param {node} parentElem Элемент, в котором производим поиск
 * @param {string} selector селектор поискового элемента
 * @param {string} innerText Текст элемента
 * @returns {Element}
 * @private
 */


var innerTextQS = function innerTextQS(parentElem, selector, innerText) {
  var element = null;
  var allElements = parentElem.querySelectorAll(selector);

  var _iterator = _createForOfIteratorHelper(allElements),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var _element = _step.value;

      _readOnlyError("element");

      if (element.innerText === innerText) {
        return element;
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
};
/**
 * Поиск элемента в Iframe
 *
 * @param {node} parentElem Элемент, в котором производим поиск
 * @param {string} selector селектор поискового элемента
 * @returns {Element}
 */


var advancedQS = function advancedQS(parentElem, selector) {
  var element = null;
  [baseQS, splitDirectChildQS].some(function (strategy) {
    element = strategy(parentElem, selector);
    return element != null;
  });
  return element;
};
/**
 * Получаем документ и отступы iframe
 * @param {Array} subSelectors Массив селекторов
 * @param {node} document
 * @returns {Object} Объект {paddingTop, paddingLeft, document}
 * @private
 */


var getParamsIframe = function getParamsIframe(subSelectors, document) {
  for (var i = 0; i < subSelectors.length; i++) {
    var path = subSelectors[i];

    try {
      var iframe = advancedQS(document, path); // @ts-ignore

      var iframeRect = iframe.getBoundingClientRect();
      var iframeStyle = window.getComputedStyle(iframe);
      return {
        top: iframeRect.top + parseFloat(iframeStyle.paddingTop),
        left: iframeRect.left + parseFloat(iframeStyle.paddingLeft),
        doc: iframe.contentDocument || iframe.contentWindow.document
      };
    } catch (e) {
      console.error(e);
    }
  }
};

var baseQS = function baseQS(parent, selector) {
  var element = null;

  try {
    element = parent.querySelector(selector);
  } catch (error) {
    console.error(error);
  }

  return element;
};

var splitDirectChildQS = function splitDirectChildQS(parent, selector) {
  try {
    var subPath = selector.split('>');
    var parentElem = parent;

    for (var i = 0; i < subPath.length; i++) {
      var path = subPath[i];
      parentElem = parentElem.querySelector(':scope >' + path);

      if (parentElem == null) {
        break;
      }
    }

    return parentElem;
  } catch (error) {// console.error(error);
  }

  return null;
};

var getElementBySelectors = function getElementBySelectors() {
  var selectors = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  // Старый вид триггеров
  if (typeof selectors === 'string') {
    selectors = [selectors];
  }

  var _iterator2 = _createForOfIteratorHelper(selectors),
      _step2;

  try {
    for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
      var selector = _step2.value;
      var target = getTargetStep(selector, false, options);

      if (target != null) {
        return getParentElementFromActionElements(target) || target;
      }
    }
  } catch (err) {
    _iterator2.e(err);
  } finally {
    _iterator2.f();
  }

  return null;
};

var getParentElementFromActionElements = function getParentElementFromActionElements(target) {
  var parent = null;
  ACTION_ELEMENTS.forEach(function (elem) {
    if (target.closest(elem)) {
      parent = target.closest(elem);
    }
  });
  return parent;
};

export { playScenario, targetElementStep };
