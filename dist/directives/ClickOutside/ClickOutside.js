function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function (obj) {
      return typeof obj;
    };
  } else {
    _typeof = function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

var HANDLER_PROPERTY = '_v-click-outside';
var ClickOutside = {
  bind: function bind(el, binding) {
    // eslint-disable-next-line no-param-reassign
    el[HANDLER_PROPERTY] = function (event) {
      var handler = binding.value;
      var middleware = true;
      var additionalElementsSelector = null;

      if (_typeof(binding.value) === 'object') {
        middleware = binding.value.middleware;
        handler = binding.value.handler;
        additionalElementsSelector = binding.value.additionalElementsSelector;
      }

      if (!middleware) {
        return;
      }
      /**
       * @type {NodeList|null}
       */


      var additionalElements = null;

      if (additionalElementsSelector) {
        additionalElements = document.querySelectorAll(additionalElementsSelector);
      } // Проверка на то, является ли DOM элемент тултипом


      var isTooltip = function isTooltip(target) {
        if (target.getAttribute('role') === 'tooltip') {
          return true;
        }

        if (target.parentElement) {
          return isTooltip(target.parentElement);
        }

        return false;
      };
      /**
       * Проверка на клик на дополнительне элементы
       * @param target Элемент, на который кликнули
       * @returns {boolean}
       */


      var isAdditionalElement = function isAdditionalElement(target) {
        if (!additionalElements) {
          return false;
        }

        for (var index = 0; index < additionalElements.length; index += 1) {
          if (additionalElements[index] === target || additionalElements[index].contains(target)) {
            return true;
          }
        }

        return false;
      }; // Если элемент, на который кликнули, уже был удален из DOM


      if (!document.body.contains(event.target)) {
        return;
      }

      if (!(el === event.target || el.contains(event.target) || isTooltip(event.target) || isAdditionalElement(event.target) || // Если элемент при клике успел пропасть из dom, то event.target будет ссылаться на body
      event.target.tagName === 'BODY')) {
        handler();
      }
    };
    /*
     Timeout необходим, чтобы не требовалось костылей, когда при клике на кнопку вне блока,
     происходит открытие самого блока, на котором повешана директива
     */


    setTimeout(function () {
      document.body.addEventListener('click', el[HANDLER_PROPERTY], true);
    }, 100);
  },
  unbind: function unbind(el) {
    document.body.removeEventListener('click', el[HANDLER_PROPERTY], true);
  },
  update: function update(el, _ref, vnode) {
    var value = _ref.value,
        oldValue = _ref.oldValue;

    if (JSON.stringify(value) === JSON.stringify(oldValue)) {
      return;
    }

    ClickOutside.unbind(el, null, vnode);
    ClickOutside.bind(el, {
      value: value
    }, vnode);
  }
};

export { ClickOutside };
