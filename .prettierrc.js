module.exports = {
  printWidth: 100,
  endOfLine: 'lf',
  singleQuote: true,
  trailingComma: 'all',
  vueIndentScriptAndStyle: true,
};
