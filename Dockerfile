FROM node:lts-alpine as build-stage

WORKDIR /app

COPY package.json yarn.lock .npmrc ./

RUN yarn

COPY . .

ENV NODE_ENV=production

RUN yarn storybook:build --quiet

FROM node:alpine as app

WORKDIR /app

RUN yarn global add http-server

EXPOSE 8080
CMD [ "http-server", "dist" ]
