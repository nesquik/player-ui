const ACTION_ELEMENTS = ['a', 'button'];

export const playScenario = (scenario, index = 0) => {
  const target = targetElementStep(scenario, index);
  
  if (!target) {
    console.error('Элемент не найден');
    return false;
  }

  console.log('Сценарий запущен');
  return target.getBoundingClientRect();
};

export const targetElementStep = (scenario, index) => {
  const step = scenario.steps[index];
  let selector = step.target || step.selector;
  let stepDescription = step.elementDescription;

  // just one target to test: check and return its results
  const experimental = step.experimental ? step.experimental : null;
  let target = getTargetStep(selector, false, experimental);
  
  if (!target && stepDescription && stepDescription.selectors) {
    let secondarySelectors = stepDescription.selectors;
    target = getElementBySelectors(stepDescription.selectors, experimental);
    
    let parentSelector = secondarySelectors[secondarySelectors.length - 1];
  
    console.log(target, 'Временный лог', 1);
  
    if (!parentSelector) {
      return null;
    }
    
    target = getTargetStep(parentSelector, false, experimental);
  
    console.log(target, 'Временный лог', 2);
  
    if (!target) {
      return null;
    }

    const elPosition = stepDescription.position;

    if (Number.isInteger(elPosition)) {
      target = target.childNodes[elPosition] || target;
    }
  }
  console.log(getParentElementFromActionElements(target), target, 'Временный лог');
  return getParentElementFromActionElements(target) || target;
};

/**
 * Получить елемент выбранный в шаге
 * @param {string} selector Селектор элемента
 * @param {boolean} [debug = true] статуст тестирования
 * @param {object} [experimental = null]
 * @returns {Document|Element}
 * @private
 */

const getTargetStep = (selector, debug = true, experimental = null) => {
  if (!selector) {
    return null;
  }

  if (experimental && experimental.useInnerText && experimental.innerText) {
    return innerTextQS(document, selector, experimental.innerText);
  }
  
  // Поиск элемента в Iframe

  if (selector.indexOf(';;') < 0) {
    return advancedQS(document, selector);
  }
  
  debug && console.log('Searching element in iframes...');
  
  const subSelectors = selector.split(';;');
  const elementSelector = subSelectors.splice(subSelectors.length - 1, 1)[0];
  let iframeDocument = document;
  let paddingTop = 0;
  let paddingLeft = 0;
  
  const { top, left, doc } = getParamsIframe(subSelectors, iframeDocument);
  
  paddingTop += top;
  paddingLeft += left;
  iframeDocument = doc;
  
  const element = advancedQS(iframeDocument, elementSelector);
  
  
  if (!element) {
    debug && console.warn(selector + ' element not found');
    
    return null;
  }
  
  element.isInsideIframe = true;
  element.elementPadding = {
    top: paddingTop,
    left: paddingLeft,
  };
  
  return element;
};

/**
 * Поиск элемент по тексту
 * @param {node} parentElem Элемент, в котором производим поиск
 * @param {string} selector селектор поискового элемента
 * @param {string} innerText Текст элемента
 * @returns {Element}
 * @private
 */
const innerTextQS = (parentElem, selector, innerText) => {
  const element = null;
  const allElements = parentElem.querySelectorAll(selector);

  for (element of allElements) {
    if (element.innerText === innerText) {
      return element;
    }
  }
};

/**
 * Поиск элемента в Iframe
 *
 * @param {node} parentElem Элемент, в котором производим поиск
 * @param {string} selector селектор поискового элемента
 * @returns {Element}
 */
const advancedQS = (parentElem, selector) => {
  let element = null;

  [baseQS, splitDirectChildQS].some((strategy) => {
    element = strategy(parentElem, selector);
    return element != null;
  });
  
  return element;
};

/**
 * Получаем документ и отступы iframe
 * @param {Array} subSelectors Массив селекторов
 * @param {node} document
 * @returns {Object} Объект {paddingTop, paddingLeft, document}
 * @private
 */
const getParamsIframe = (subSelectors, document) => {
  for (let i = 0; i < subSelectors.length; i++) {
    const path = subSelectors[i];
    try {
      const iframe = advancedQS(document, path);
      // @ts-ignore
      const iframeRect = iframe.getBoundingClientRect();
      const iframeStyle = window.getComputedStyle(iframe);
      
      return {
        top: iframeRect.top + parseFloat(iframeStyle.paddingTop),
        left: iframeRect.left + parseFloat(iframeStyle.paddingLeft),
        doc: iframe.contentDocument || iframe.contentWindow.document,
      };
    } catch (e) {
      console.error(e);
    }
  }
};


const baseQS = (parent, selector) => {
  let element = null;
  
  try {
    element = parent.querySelector(selector);
  } catch (error) {
    console.error(error);
  }
  
  return element;
};

const splitDirectChildQS = (parent, selector) => {
  try {
    const subPath = selector.split('>');
    let parentElem = parent;
    for (let i = 0; i < subPath.length; i++) {
      const path = subPath[i];
      
      parentElem = parentElem.querySelector(':scope >' + path);
      
      if (parentElem == null) {
        break;
      }
    }
    return parentElem;
  } catch (error) {
    // console.error(error);
  }
  
  return null;
};

const getElementBySelectors = (selectors = [], options = {}) => {
  // Старый вид триггеров
  if (typeof selectors === 'string') {
    selectors = [selectors]
  };
  
  for (let selector of selectors) {
    const target = getTargetStep(selector, false, options);

    if (target != null) {
      return getParentElementFromActionElements(target) || target;
    }
  }
  return null;
};

const getParentElementFromActionElements = (target) => {
  let parent = null;
  
  ACTION_ELEMENTS.forEach((elem) => {
    if (target.closest(elem)) {
      parent = target.closest(elem);
    }
  });
  
  return parent;
};