import { ClickOutside } from './ClickOutside';

export default {
  title: 'Directives/ClickOutside',
  argTypes: {},
  parameters: { docs: { source: { type: 'code' } } },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),

  directives: { 'click-outside': ClickOutside },

  computed: {
    clickOutsideConfig() {
      return {
        middleware: true,
        handler: () => {
          // eslint-disable-next-line no-alert
          alert('Click outside --- closed');
        },
      };
    },
  },

  template: `<div style="height: 100vh; width: 100vw; display: flex; align-items: center; justify-content: center">
    <div v-click-outside="clickOutsideConfig" style="height: 150px; width: 300px; display: flex; align-items: center; justify-content: center; background: red">Modal(Click out)</div>
  </div>`,
});

export const Default = Template.bind({});
Default.args = {};

const CustomSelector = (args, { argTypes }) => ({
  props: Object.keys(argTypes),

  directives: { 'click-outside': ClickOutside },

  computed: {
    clickOutsideConfig() {
      return {
        middleware: true,
        handler: () => {
          // eslint-disable-next-line no-alert
          alert('Click outside --- closed');
        },
        additionalElementsSelector: '#custom-element',
      };
    },
  },

  template: `<div style="height: 100vh; width: 100vw; display: flex; align-items: center; justify-content: center">
    <div v-click-outside="clickOutsideConfig" style="height: 150px; width: 300px; display: flex; align-items: center; justify-content: center; background: red">Modal(Click out)</div>
    <div id="custom-element" style="height: 50px; width: 100px; display: flex; align-items: center; justify-content: center; margin-left: 5px; background: green">Custom element</div>
  </div>`,
});

export const WithCustomSelector = CustomSelector.bind({});
WithCustomSelector.args = {};
