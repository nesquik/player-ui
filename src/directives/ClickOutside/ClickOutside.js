const HANDLER_PROPERTY = '_v-click-outside';

export const ClickOutside = {
  bind(el, binding) {
    // eslint-disable-next-line no-param-reassign
    el[HANDLER_PROPERTY] = (event) => {
      let handler = binding.value;
      let middleware = true;
      let additionalElementsSelector = null;

      if (typeof binding.value === 'object') {
        middleware = binding.value.middleware;
        handler = binding.value.handler;

        additionalElementsSelector = binding.value.additionalElementsSelector;
      }

      if (!middleware) {
        return;
      }

      /**
       * @type {NodeList|null}
       */
      let additionalElements = null;
      if (additionalElementsSelector) {
        additionalElements = document.querySelectorAll(additionalElementsSelector);
      }

      // Проверка на то, является ли DOM элемент тултипом
      const isTooltip = (target) => {
        if (target.getAttribute('role') === 'tooltip') {
          return true;
        }

        if (target.parentElement) {
          return isTooltip(target.parentElement);
        }

        return false;
      };

      /**
       * Проверка на клик на дополнительне элементы
       * @param target Элемент, на который кликнули
       * @returns {boolean}
       */
      const isAdditionalElement = (target) => {
        if (!additionalElements) {
          return false;
        }

        for (let index = 0; index < additionalElements.length; index += 1) {
          if (additionalElements[index] === target || additionalElements[index].contains(target)) {
            return true;
          }
        }

        return false;
      };

      // Если элемент, на который кликнули, уже был удален из DOM
      if (!document.body.contains(event.target)) {
        return;
      }

      if (
        !(
          el === event.target ||
          el.contains(event.target) ||
          isTooltip(event.target) ||
          isAdditionalElement(event.target) ||
          // Если элемент при клике успел пропасть из dom, то event.target будет ссылаться на body
          event.target.tagName === 'BODY'
        )
      ) {
        handler();
      }
    };

    /*
     Timeout необходим, чтобы не требовалось костылей, когда при клике на кнопку вне блока,
     происходит открытие самого блока, на котором повешана директива
     */
    setTimeout(() => {
      document.body.addEventListener('click', el[HANDLER_PROPERTY], true);
    }, 100);
  },

  unbind(el) {
    document.body.removeEventListener('click', el[HANDLER_PROPERTY], true);
  },

  update(el, { value, oldValue }, vnode) {
    if (JSON.stringify(value) === JSON.stringify(oldValue)) {
      return;
    }

    ClickOutside.unbind(el, null, vnode);

    ClickOutside.bind(el, { value }, vnode);
  },
};
