module.exports = {
  '**/*.{js,vue}': ['eslint --fix'],
  '**/*.{md,mdx,css,yaml,yml,scss}': ['prettier --write'],
};
